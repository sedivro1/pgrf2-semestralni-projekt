package models;

public class SnowFlake {
    private boolean isAlive = true;
    private float x;
    private float y;
    private float z;
    private float speed;

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public SnowFlake(float x, float y, float z, float speed){
        this.x = x;
        this.y = y;
        this.z = z;
        this.speed = speed;
    }

}
