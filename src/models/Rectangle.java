package models;

import transforms.Point3D;

//Musíme zadávat hodnoty podle směru hod ručiček
public class Rectangle {
    Point3D[] rectBoundaries = new Point3D[4];

    public Point3D[] getRectBoundaries() {
        return rectBoundaries;
    }

    public Rectangle(Point3D[] point3DS){
        rectBoundaries[0] = point3DS[0];
        rectBoundaries[1] = point3DS[1];
        rectBoundaries[2] = point3DS[2];
        rectBoundaries[3] = point3DS[3];
    }
}
