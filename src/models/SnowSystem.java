package models;

import transforms.Point3D;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SnowSystem {
    List<SnowFlake> snowFlakeList;
    List<SnowFlake> fallenSnowFlakes = new ArrayList<>();
    Random random = new Random();
    List<Rectangle> rectangles;


    public SnowSystem(List<Rectangle> rectangles, List<SnowFlake> snowFlakes) {
        this.snowFlakeList = snowFlakes;
        this.rectangles = rectangles;
    }

    public void fallenSnowFlakes(){
        for (SnowFlake snowFlake: snowFlakeList){
            if (!snowFlake.isAlive()){
                fallenSnowFlakes.add(new SnowFlake(snowFlake.getX(),snowFlake.getY()+0.01f,snowFlake.getZ(),random.nextFloat()));
                snowFlake.setAlive(true);
                snowFlake.setY(50);
                snowFlake.setX(0 + random.nextFloat() * (50 - 0));
                snowFlake.setZ(0 + random.nextFloat() * (50 - 0));
            }
        }
    }


    public List<SnowFlake> getSnowFlakeList() {
        return snowFlakeList;
    }

    public List<SnowFlake> getFallenSnowFlakes() {
        return fallenSnowFlakes;
    }


    public boolean collide(SnowFlake snowFlake, Rectangle cube){
        Point3D[] boundaries = cube.getRectBoundaries();
        if (snowFlake.getX()>boundaries[0].getX()&&snowFlake.getZ()>boundaries[0].getZ()&&
                snowFlake.getX()>boundaries[1].getX()&&snowFlake.getZ()<boundaries[1].getZ()&&
                snowFlake.getX()<boundaries[2].getX()&&snowFlake.getZ()<boundaries[2].getZ()&&
                snowFlake.getX()<boundaries[3].getX()&&snowFlake.getZ()>boundaries[3].getZ() && snowFlake.getY()<=boundaries[0].getY())
            return true;

        return false;

    }

    public void updateSnowFlake(){
        for (SnowFlake snowFlake: snowFlakeList) {
            for (Rectangle cube: rectangles) {
                if (collide(snowFlake, cube)) {
                    snowFlake.setAlive(false);
                } else {
                    snowFlake.setY(snowFlake.getY() - snowFlake.getSpeed());

                }
            }
        }
    }


}



