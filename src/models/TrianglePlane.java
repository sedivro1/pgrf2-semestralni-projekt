package models;

import transforms.Point3D;

public class TrianglePlane {
    private int width;
    private int height;
    private Point3D[][] point3DS;

    public int getHeight() {
        return height;
    }

    public Point3D[][] getPoint3DS() {
        return point3DS;
    }

    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public TrianglePlane(int height, int width){
        this.width = width;
        this.height = height;
        point3DS = generateTriangleStripGrid(width,height);
    }

    //Vygerování jednotlivých bodů pro TRIANGLE_STRIP mřížku
    private Point3D[][] generateTriangleStripGrid(int height,int width){
        Point3D [][] points = new Point3D[height][width];

        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++) {
                //Generace bodů
                points[i][j] = new Point3D(j, 0, i);
            }
        }
        return points;
    }
}
