package Rasterizer;

import lwjglutils.OGLTextRenderer;
import lwjglutils.OGLTexture2D;
import models.*;
import models.Rectangle;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import transforms.Point3D;
import utils.AbstractRenderer;
import utils.GLCamera;

import java.io.IOException;
import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL20.*;
import static utils.GluUtils.gluLookAt;
import static utils.GluUtils.gluPerspective;
import static utils.GlutUtils.glutSolidCube;

public class Renderer extends AbstractRenderer {


    private final int MAX_PARTICLES = 1000;
    private SnowFlake[] snowFlakes = new SnowFlake[MAX_PARTICLES];
    private float trans, deltaTrans = 0;
    private float px, py=10, pz;
    private double ex, ey, ez;
    private boolean mouseButton1 = false;
    private float dx, dy, ox, oy;
    private float zenit=0.5625f, azimut=-231.75f;
    private GLCamera camera;
    OGLTexture2D texture1,texture2,texture3;
    TrianglePlane trianglePlane;
    SnowSystem snowSystem;
    Point3D [] [] triangleStripGrid;
    Random random = new Random();

    public Renderer(){
    super();
        glfwKeyCallback = new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
                    // We will detect this in our rendering loop
                    glfwSetWindowShouldClose(window, true);
                    switch (key) {
                        case GLFW_KEY_W:
                            trans = 0.5f;
                            px += ex * trans;
                            py += ey * trans;
                            pz += ez * trans;
                            camera.forward(trans);
                            break;

                        case GLFW_KEY_S:
                            trans = 0.5f;
                            px -= ex * trans;
                            py -= ey * trans;
                            pz -= ez * trans;
                            camera.forward(trans);
                            break;

                        case GLFW_KEY_A:
                            trans = 0.5f;
                            pz -= Math.cos(azimut * Math.PI / 180 - Math.PI / 2) * trans;
                            px += Math.sin(azimut * Math.PI / 180 - Math.PI / 2) * trans;
                            camera.left(trans);
                            break;

                        case GLFW_KEY_D:
                            trans = 0.5f;
                            pz += Math.cos(azimut * Math.PI / 180 - Math.PI / 2) * trans;
                            px -= Math.sin(azimut * Math.PI / 180 - Math.PI / 2) * trans;
                            camera.right(trans);
                            break;
                    }
                }

        };



    glfwMouseButtonCallback = new GLFWMouseButtonCallback() {

        @Override
        public void invoke(long window, int button, int action, int mods) {

            DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
            DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
            glfwGetCursorPos(window, xBuffer, yBuffer);
            double x = xBuffer.get(0);
            double y = yBuffer.get(0);

            mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS;

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
                ox = (float) x;
                oy = (float) y;
            }
        }

    };

    glfwCursorPosCallback = new GLFWCursorPosCallback() {
        @Override
        public void invoke(long window, double x, double y) {
            if (mouseButton1) {
                dx = (float) x - ox;
                dy = (float) y - oy;
                ox = (float) x;
                oy = (float) y;
                zenit -= dy / width * 180;
                if (zenit > 90)
                    zenit = 90;
                if (zenit <= -90)
                    zenit = -90;
                azimut += dx / height * 180;
                azimut = azimut % 360;
                camera.setAzimuth(Math.toRadians(azimut));
                camera.setZenith(Math.toRadians(zenit));
                dx = 0;
                dy = 0;
            }
        }
    };
}

    @Override
    public void init() {
        //GL fixed pipeline settings
        textRenderer = new OGLTextRenderer(width, height);
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        gluPerspective(45, width / (float) height, 0.1f, 100.0f);
        //Bleding
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        //
        //Vyplnění
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_FILL);
        ////
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        camera = new GLCamera();
        //Point sprite pro vykreslování textury na body
        glEnable(GL_POINT_SPRITE);
        //Zajistí transparentnost u vloček
        glAlphaFunc(GL_GREATER, 0.1f);
        glEnable(GL_ALPHA_TEST);
        ///////////////////////////////////////////////


        //Import textur
        try {
            texture1 = new OGLTexture2D("textures/grass.png");
            texture2 = new OGLTexture2D("textures/snowflake.png");
            texture3 = new OGLTexture2D("textures/brick.png");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //////////////////////////////////////////////////////////////
        //Vytvoření kolizních čtverců
        List<Rectangle> rectangles = new ArrayList<>();


        //Příprava scény
        trianglePlane = new TrianglePlane(50,50);
        rectangles.add(new Rectangle(new Point3D[]{new Point3D(0,0,0),new Point3D(0,0,trianglePlane.getHeight()-1),new Point3D(trianglePlane.getWidth(),0,trianglePlane.getHeight()-1),new Point3D(trianglePlane.getWidth(),0,0),}));
        rectangles.add(new Rectangle(new Point3D[]{new Point3D(27.5,5,7.5),new Point3D(27.5,5,12.5),new Point3D(32.5,5,12.5),new Point3D(32.5,5,7.5),}));
        triangleStripGrid = trianglePlane.getPoint3DS();
        for (int i = 0; i < MAX_PARTICLES; i++) {
            snowFlakes[i] = new SnowFlake(0 + random.nextFloat() * (50 - 0), 0 + random.nextFloat() * (50 - 0), 0 + random.nextFloat() * (50 - 0),0.00004f + random.nextFloat() * (0.00011f - 0.00004f));
        }
        snowSystem = new SnowSystem(rectangles,Arrays.asList(snowFlakes));
        //////////////////////////////////////////////////////////////

    }


    public void drawScene(){
        //Použití mapování textury na bod
        glEnable(GL_TEXTURE_2D);
        glTexEnvi(GL_POINT_SPRITE,GL_COORD_REPLACE,GL_TRUE);
        texture2.bind();
        glPointSize(20);
        glBegin(GL_POINTS);
        glDrawArrays(GL_POINTS,0,540);
        for (SnowFlake snowFlake : snowSystem.getSnowFlakeList()) {
            glVertex3f(snowFlake.getX(), snowFlake.getY(), snowFlake.getZ());
            snowSystem.updateSnowFlake();
            snowSystem.fallenSnowFlakes();
        }
        glEnd();

        glDisable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
        for (SnowFlake snowFlake : snowSystem.getFallenSnowFlakes()) {
            glVertex3f(snowFlake.getX()-0.25f, snowFlake.getY(), snowFlake.getZ()-0.25f);
            glVertex3f(snowFlake.getX()-0.25f, snowFlake.getY(), snowFlake.getZ()+0.25f);
            glVertex3f(snowFlake.getX()+0.25f, snowFlake.getY(), snowFlake.getZ()+0.25f);
            glVertex3f(snowFlake.getX()+0.25f, snowFlake.getY(), snowFlake.getZ()-0.25f);

        }
        glEnd();

        glEnable(GL_TEXTURE_2D);
        glTexEnvi(GL_POINT_SPRITE,GL_COORD_REPLACE,GL_FALSE);
        texture1.bind();
        glColor3f(1f,1f,1f);
        for(int i=0;i<trianglePlane.getHeight()-1;i++){
            glBegin(GL_TRIANGLE_STRIP);
            int mipMappingCoef= 5;
            float t0 = (float) i*mipMappingCoef/(trianglePlane.getHeight()-1);
            float t1 = (float) (i+1)*mipMappingCoef / (trianglePlane.getHeight()-1);

            for(int j= 0; j<trianglePlane.getWidth();j++) {
                float s = (float)j*mipMappingCoef / (trianglePlane.getWidth() - 1);

                    glTexCoord2f(s,t0);
                    glVertex3d(triangleStripGrid[i][j].getX(), triangleStripGrid[i][j].getY(), triangleStripGrid[i][j].getZ());
                    glTexCoord2f(s,t1);
                    glVertex3d(triangleStripGrid[i+1][j].getX(), triangleStripGrid[i+1][j].getY(), triangleStripGrid[i+1][j].getZ());
            }
            glEnd();
        }


        texture3.bind();
        glPushMatrix();
        glTranslated(30,2.5,10);
        glutSolidCube(5f);
        glPopMatrix();



    }

    @Override
    public void display() {

            glViewport(0, 0, width, height);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_TEXTURE_2D);
            glActiveTexture(GL_TEXTURE0);


            drawScene();

            //Ovládání kamery
            trans += deltaTrans;
            double a_rad = azimut * Math.PI / 180;
            double z_rad = zenit * Math.PI / 180;
            ex = Math.sin(a_rad) * Math.cos(z_rad);
            ey = Math.sin(z_rad);
            ez = -Math.cos(a_rad) * Math.cos(z_rad);
            double ux = Math.sin(a_rad) * Math.cos(z_rad + Math.PI / 2);
            double uy = Math.sin(z_rad + Math.PI / 2);
            double uz = -Math.cos(a_rad) * Math.cos(z_rad + Math.PI / 2);
            glLoadIdentity();
            gluLookAt(px, py, pz, ex + px, ey + py, ez + pz, ux, uy, uz);
            ////////////////////////////////////

            //Textové komentáře
            String text = this.getClass().getName() + ": Pohyb: [WASD] "+"Počet vloček ve vzduchu: "+MAX_PARTICLES+" Počet vloček na zemi: "+ snowSystem.getFallenSnowFlakes().size();
            textRenderer.addStr2D(width - 190, height - 3, " (c) Roman Šedivý UHK 30.4.2021");
            textRenderer.addStr2D(3, 20, text);
            ///////////////////////////////////////////////////////

            //Zprocesuj všechny čekající eventy
            glfwPollEvents();

    }


}
