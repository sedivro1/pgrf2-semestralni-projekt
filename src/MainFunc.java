
import utils.AbstractRenderer;
import utils.LwjglWindow;



public class MainFunc extends AbstractRenderer {

    public static void main(String[] args) {
        new LwjglWindow(new Rasterizer.Renderer());
    }

}
